package com.akvelon.admir;

import com.akvelon.admir.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");// spring container
        UserService userService = context.getBean(UserService.class);
        userService.signUp("sidikov.marsel@akvelon.com", "qwerty007");
    }
}
