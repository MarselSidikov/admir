package com.akvelon.admir;

import com.akvelon.admir.service.UsersServiceImpl;
import com.akvelon.admir.validation.EmailRegexValidatorImpl;
import com.akvelon.admir.validation.EmailSimpleValidatorImpl;

public class Main {
    public static void main(String[] args) {
        EmailSimpleValidatorImpl simpleEmailValidator = new EmailSimpleValidatorImpl("@.&");
        EmailRegexValidatorImpl emailRegexValidator = new EmailRegexValidatorImpl();
        emailRegexValidator.setRegex("^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
        UsersServiceImpl usersService = new UsersServiceImpl("users.txt", emailRegexValidator);
        usersService.signUp("haha@gmail.com", "qwerty007");
    }
}