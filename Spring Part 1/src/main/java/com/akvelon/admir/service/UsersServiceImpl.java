package com.akvelon.admir.service;

import com.akvelon.admir.validation.EmailValidator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class UsersServiceImpl implements UserService {
    private String fileName;

    private EmailValidator emailValidator;


    public UsersServiceImpl(String fileName, EmailValidator emailValidator) {
        this.emailValidator = emailValidator;
        this.fileName = fileName;
    }

    public void signUp(String email, String password) {
        emailValidator.validate(email);
        // try-with-resources
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String userToSave = email + "|" + password;
            bufferedWriter.write(userToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
