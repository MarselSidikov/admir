package com.akvelon.admir.validation;

public class EmailSimpleValidatorImpl implements EmailValidator {
    private String characters; // @.

    public EmailSimpleValidatorImpl(String characters) {
        this.characters = characters;
    }

    public void validate(String email) {
        char[] charactersAsArray = characters.toCharArray();
        for (char character : charactersAsArray) {
            if (email.indexOf(character) == -1) {
                throw new IllegalArgumentException("Wrong email");
            }
        }
    }
}
