package com.akvelon.admir.validation;

public interface EmailValidator {
    void validate(String email);
}
