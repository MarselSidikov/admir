package com.akvelon.admir.validation;

import java.util.regex.Pattern;

public class EmailRegexValidatorImpl implements EmailValidator {

    private String regex;

    @Override
    public void validate(String email) {
        if (!Pattern.compile(regex).matcher(email).matches()) {
            throw new IllegalArgumentException("Incorrect email format");
        }
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }
}
