# Spring 

* IoC - pattern, that allows you describes your components in the another module, not in your program
* Spring - IoC Framework
* DI - Dependency Injection - pattern, for inject one component (bean) into another component (bean)
  * constructor injection
  * setter injection
  * field injection