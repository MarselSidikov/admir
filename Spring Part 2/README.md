* `@Autowired`
* `@Qualifier`
* `@Value`
* `@Component`


@Component
-> Creating the object of class -> put into the container

@Bean
-> Run the Bean method -> some instructions -> put into the container