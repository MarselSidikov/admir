package com.akvelon.admir;

import com.akvelon.admir.config.ApplicationConfig;
import com.akvelon.admir.randomizer.RandomNumber;
import com.akvelon.admir.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);// spring container
        UserService userService = context.getBean(UserService.class);
        userService.signUp("sidikov.marsel@akvelon.com", "qwerty007");

        RandomNumber randomNumber = context.getBean(RandomNumber.class);
        System.out.println(randomNumber.getValue());
    }
}
