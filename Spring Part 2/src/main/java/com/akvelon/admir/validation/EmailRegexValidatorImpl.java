package com.akvelon.admir.validation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component("regexEmailValidator")
public class EmailRegexValidatorImpl implements EmailValidator {

    private final String regex;

    public EmailRegexValidatorImpl(@Value("${email.validator.regex}") String regex) {
        this.regex = regex;
    }

    @Override
    public void validate(String email) {
        if (!Pattern.compile(regex).matcher(email).matches()) {
            throw new IllegalArgumentException("Incorrect email format");
        }
    }
}
