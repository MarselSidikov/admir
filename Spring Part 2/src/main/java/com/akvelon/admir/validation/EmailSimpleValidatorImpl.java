package com.akvelon.admir.validation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

// emailSimpleValidatorImpl
@Component("simpleEmailValidator")
public class EmailSimpleValidatorImpl implements EmailValidator {
    private String characters;

    public EmailSimpleValidatorImpl(@Value("email.validator.characters") String characters) {
        this.characters = characters;
    }

    public void validate(String email) {
        char[] charactersAsArray = characters.toCharArray();
        for (char character : charactersAsArray) {
            if (email.indexOf(character) == -1) {
                throw new IllegalArgumentException("Wrong email");
            }
        }
    }
}
