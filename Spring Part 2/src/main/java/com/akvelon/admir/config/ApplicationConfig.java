package com.akvelon.admir.config;

import com.akvelon.admir.randomizer.RandomNumber;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Random;
import java.util.Scanner;

@PropertySource("classpath:application.properties")
@ComponentScan("com.akvelon.admir")
@Configuration
public class ApplicationConfig {

    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }

    @Bean
    public RandomNumber number() {
        Random random = new Random();
        RandomNumber number = new RandomNumber(random.nextInt(100));
        return number;
    }
}
