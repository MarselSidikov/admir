package com.akvelon.admir.service;

import com.akvelon.admir.validation.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

@Component
public class UsersServiceImpl implements UserService {

    private final String fileName;

    private final EmailValidator emailValidator;

    @Autowired
    public UsersServiceImpl(@Value("${users.service.file}") String fileName, @Qualifier("regexEmailValidator") EmailValidator emailValidator) {
        this.fileName = fileName;
        this.emailValidator = emailValidator;
    }

    public void signUp(String email, String password) {
        emailValidator.validate(email);
        // try-with-resources
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String userToSave = email + "|" + password;
            bufferedWriter.write(userToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
