package com.akvelon.admir.randomizer;

import org.springframework.stereotype.Component;

public class RandomNumber {
    private int value;

    public RandomNumber(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
